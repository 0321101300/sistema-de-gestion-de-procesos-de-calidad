from django.db import models

# Create your models here.

class Auditor(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    correo = models.CharField(max_length=100)
    telefono = models.CharField(max_length=10)
    

    def _str_(self):
        return f"{self.nombre} {self.apellido}"

class Aplicacion(models.Model):
    nombre = models.CharField(max_length=200)
    # Otros campos relacionados con la aplicación, como versión, plataforma, etc.

    def _str_(self):
        return self.nombre

class Tarea(models.Model):
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_limite = models.DateTimeField()
    completada = models.BooleanField(default=False)
    auditor_asignado = models.ForeignKey(Auditor, on_delete=models.CASCADE)
    aplicacion_relacionada = models.ForeignKey(Aplicacion, on_delete=models.CASCADE)
    
    def _str_(self):
        return self.descripcion