from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from home import models, forms


# Create your views here.
##LIST
class index(generic.ListView):
    template_name = 'list_exam.html'
    model = models.Tarea

    def get(self, request):
        self.context = {
            "tarea": models.Tarea.objects.filter()
        }
        return render(request, self.template_name,  self.context)
    

##CREATE
class NewTarea(generic.CreateView):
    template_name = 'create_exam.html'
    model = models.Tarea
    form_class = forms.CreateTarea
    success_url = reverse_lazy('home:list_exam')
    
    
##UPDATE
class UpdateTarea(generic.UpdateView):
    template_name = 'update_exam.html'
    model = models.Tarea
    form_class = forms.UpdateTarea
    success_url = reverse_lazy('home:list_exam')


##DELETE
class DeleteTarea(generic.DeleteView):
    template_name = 'delete_exam.html'
    model = models.Tarea
    success_url = reverse_lazy('home:list_exam')
    
    
    
    
################ Auditor #######################
##LIST
class ListAuditor(generic.ListView):
    template_name = 'list_auditar.html'
    model = models.Auditor

    def get(self, request):
        self.context = {
            "auditor": models.Auditor.objects.filter()
        }
        return render(request, self.template_name,  self.context)
    

##CREATE
class NewAuditor(generic.CreateView):
    template_name = 'create_auditar.html'
    model = models.Auditor
    form_class = forms.NewAuditor
    success_url = reverse_lazy('home:list_auditoria')
    
    
##UPDATE
class UpdateAuditor(generic.UpdateView):
    template_name = 'update_auditar.html'
    model = models.Auditor
    form_class = forms.UpdateAuditor
    success_url = reverse_lazy('home:list_auditoria')


##DELETE
class DeleteAuditor(generic.DeleteView):
    template_name = 'delete_auditar.html'
    model = models.Auditor
    success_url = reverse_lazy('home:list_auditoria')

 
################ Aplicacion #######################
##LIST
class ListAplicacion(generic.ListView):
    template_name = 'list_aplication.html'
    model = models.Aplicacion

    def get(self, request):
        self.context = {
            "Aplicacion": models.Aplicacion.objects.filter()
        }
        return render(request, self.template_name,  self.context)
    

##CREATE
class NewAplicacion(generic.CreateView):
    template_name = 'create_aplication.html'
    model = models.Aplicacion
    form_class = forms.NewAplicacion
    success_url = reverse_lazy('home:list_aplication')
    
    
##UPDATE
class UpdateAplicacion(generic.UpdateView):
    template_name = 'update_aplication.html'
    model = models.Aplicacion
    form_class = forms.UpdateAplicacion
    success_url = reverse_lazy('home:list_aplication')


##DELETE
class DeleteAplicacion(generic.DeleteView):
    template_name = 'delete_aplication.html'
    model = models.Aplicacion
    success_url = reverse_lazy('home:list_aplication')



    

    