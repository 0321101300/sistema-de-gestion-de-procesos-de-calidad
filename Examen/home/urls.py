from django.urls import path
from home import views

app_name = 'home'

urlpatterns = [
    path('',views.index.as_view(), name = 'list_exam'),
    path('new/tarea/',views.NewTarea.as_view(), name = 'create_exam'),
    path('update/tarea/<int:pk>/',views.UpdateTarea.as_view(), name = 'update_exam'),
    path('delete/tarea/<int:pk>/',views.DeleteTarea.as_view(),name = 'delete_exam'),
    
    ###### Auditor URL
    path('list/auditor/',views.ListAuditor.as_view(), name = 'list_auditoria'),
    path('new/auditor/',views.NewAuditor.as_view(), name = 'create_auditoria'),
    path('update/auditor/<int:pk>/',views.UpdateAuditor.as_view(), name = 'update_auditoria'),
    path('delete/auditor/<int:pk>/',views.DeleteAuditor.as_view(),name = 'delete_auditoria'),
    
    
    ###### Aplicacion URL
    path('list/aplicacion',views.ListAplicacion.as_view(), name = 'list_aplication'),
    path('new/aplicacion/',views.NewAplicacion.as_view(), name = 'create_aplication'),
    path('update/aplicacion/<int:pk>/',views.UpdateAplicacion.as_view(), name = 'update_aplication'),
    path('delete/aplicacion/<int:pk>/',views.DeleteAplicacion.as_view(),name = 'delete_aplication'),
]