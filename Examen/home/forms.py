from django import forms
from home import models

class CreateTarea(forms.ModelForm):
    class Meta:
        model = models.Tarea
        fields = {
            'descripcion',
            # 'fecha_creacion',
            'fecha_limite',
            'completada',
            'auditor_asignado',
            'aplicacion_relacionada'
        }
        widgets = {
                'auditor_asignado':forms.Select(attrs={"type":"select", "class":"form-control"}),
                'aplicacion_relacionada':forms.Select(attrs={"type":"select", "class":"form-control"}),
                'descripcion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Descripción de la tarea'}),
                # 'fecha_creacion': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
                'fecha_limite': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
                'completada': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }
    
class UpdateTarea(forms.ModelForm):
    class Meta:
        model = models.Tarea
        fields = {
            'descripcion',
            # 'fecha_creacion',
            'fecha_limite',
            'completada',
            'auditor_asignado',
            'aplicacion_relacionada'
        }
        widgets = {
                'auditor_asignado':forms.Select(attrs={"type":"select", "class":"form-control"}),
                'aplicacion_relacionada':forms.Select(attrs={"type":"select", "class":"form-control"}),
                'descripcion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Descripción de la tarea'}),
                # 'fecha_creacion': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
                'fecha_limite': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
                'completada': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }


class NewAuditor(forms.ModelForm):
    class Meta:
        model = models.Auditor
        fields = ['nombre', 'apellido', 'correo', 'telefono']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'correo': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Teléfono'}),
        }

class UpdateAuditor(forms.ModelForm):
    class Meta:
        model = models.Auditor
        fields = ['nombre', 'apellido', 'correo', 'telefono']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'correo': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Teléfono'}),
        }


class NewAplicacion(forms.ModelForm):
    class Meta:
        model = models.Aplicacion
        fields = ['nombre']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la aplicación'}),
        }        
    
class UpdateAplicacion(forms.ModelForm):
    class Meta:
        model = models.Aplicacion
        fields = ['nombre']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la aplicación'}),
        }        
    
    
 