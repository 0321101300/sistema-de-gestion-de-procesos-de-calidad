from django.contrib import admin

from home import models
# Register your models here.

@admin.register(models.Tarea)
class TareaAdmin(admin.ModelAdmin):
    list_display = [
        'descripcion',
        'fecha_creacion',
        'fecha_limite',
        'completada',
        'auditor_asignado',
        'aplicacion_relacionada'        
    ]

@admin.register(models.Auditor)
class AuditoriaAdmin(admin.ModelAdmin):
    list_display=[
        'nombre',
        'apellido',
        'correo',
        'telefono'
    ]

@admin.register(models.Aplicacion)
class AplicacionAdmin(admin.ModelAdmin):
    list_display=[
        'nombre'
    ]
      
